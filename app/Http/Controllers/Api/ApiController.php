<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;

class ApiController extends Controller
{
    public function getPosts()
    {
    	return Post::with('user','tags')->published()->paginate(5)->toArray();
    }

    public function getPostBySlug($postSlug)
    {
    	return Post::with('user','tags')->where('slug', '=', $postSlug)->firstOrFail();
    }
}
