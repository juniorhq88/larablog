<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Tag;
use App\Http\Requests\Admin\PostRequest;
use Laracasts\Flash\Flash;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id','DESC')->get();
        //dd($posts[5]->tags);
        return view('admin.posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::get()->pluck('name', 'id');
      return view('admin.posts.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $request->all();

        $post = Post::create([
                'title' => $data['title'],
                'body' => $data['body'],
                'published_at' => $data['published_at'],
                'user_id' => $data['user_id'],
            ]);

        
        if(is_array($data['tag']) && $data['tag'] != null){
            $post->tags()->attach($data['tag']);
            /*foreach ($data['tag'] as $tags) {
                $tag = Tag::find($tags);
                $post->tags()->save($tag);
            }*/  
        }
        

        if($post){
             Flash::success('Post Creado correctamente');
        }else{
            Flash::error('Fallo al crear el Post');
        }

       // return redirect()->route('admin.post.index');
        return response()->json(array('status' => 'success','msg' => 'Elemento Enviado correctamente'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
         $post = Post::find($post->id);
         $tags = Tag::get()->pluck('name', 'id');

        return view('admin.posts.edit', compact('post','tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function update(Post $post, PostRequest $request)
    {
       $data = $request->all();

        $post = Post::find($post->id);

        $post->fill($data);
        $post->save(); 

        if(is_array($data['tag']) && $data['tag'] != null){
            $post->tags()->sync($data['tag']);
        }

        if($post){
             Flash::success('Post Actualizado correctamente');
        }else{
            Flash::error('Fallo al actualizar el Post');
        }
        
        //return redirect()->route('admin.post.index');
        return response()->json(array('status' => 'success','msg' => 'Elemento Actualizado correctamente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $post = Post::findOrFail($id);
         $post->delete();
          Flash::success('Post Eliminado correctamente');
       // return redirect()->route('admin.post.index');
          return response()->json(array('status' => 'success','msg' => 'Elemento Eliminado correctamente'));
    }
}
