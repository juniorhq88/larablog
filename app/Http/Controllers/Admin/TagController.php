<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;
use App\Http\Requests\Admin\TagRequest;
use Laracasts\Flash\Flash;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $tags = Tag::orderBy('id','DESC')->get();
        return view('admin.tags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->all();

        $tag = Tag::create([
                'name' => $data['name']
            ]);

        if($tag){
             Flash::success('Tag Creada correctamente');
        }else{
            Flash::error('Fallo al crear la Tag');
        }

       // return redirect()->route('admin.tag.index');
        return response()->json(array('status' => 'success','msg' => 'Elemento Enviado correctamente'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
        $tag = Tag::find($tag->id);

        return view('admin.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Tag $tag, TagRequest $request)
    {

       $data = $request->all();

        $tag = Tag::find($tag->id);

        $tag->fill($data);
        $tag->save(); 

        if($tag){
             Flash::success('Tag Actualizada correctamente');
        }else{
            Flash::error('Fallo al actualizar la tag');
        }
        
        //return redirect()->route('admin.tag.index');
         return response()->json(array('status' => 'success','msg' => 'Elemento Actualizado correctamente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
         $tag->delete();
         Flash::success('Tag Eliminada correctamente');
         return response()->json(array('status' => 'success'));
       // return redirect()->route('admin.tag.index');
    }
}
