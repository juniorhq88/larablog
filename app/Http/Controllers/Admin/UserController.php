<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use Laracasts\Flash\Flash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('id','DESC')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
       $data = $request->all();

        $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => $data['password'],
            ]);

        if($user){
             Flash::success('Usuario Creado correctamente');
        }else{
            Flash::error('Fallo al crear el usuario');
        }

       // return redirect()->route('admin.user.index');
        return response()->json(array('status' => 'success','msg' => 'Elemento Enviado correctamente'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::find($user->id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        
         /*if (empty($request->get('password'))) {
            $request->except('password');
            }*/
       $data = $request->all();

        $user = User::find($user->id);

        $user->fill($data);
        $user->save(); 

        if($user){
             Flash::success('Usuario Actualizado correctamente');
        }else{
            Flash::error('Fallo al actualizar el usuario');
        }
        
        //return redirect()->route('admin.user.index');
        return response()->json(array('status' => 'success','msg' => 'Elemento Actualizado correctamente'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $user = User::findOrFail($id);

        //if($user->id != Auth()->user->id){
         $user->delete();
          Flash::success('Usuario Eliminado correctamente');
      //}
       // return redirect()->route('admin.user.index');
      return response()->json(array('status' => 'success','msg' => 'Elemento Eliminado correctamente'));
    }
}
