<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Taggable extends Model
{
    protected $fillable = ['tag_id','taggable_id','taggable_type'];
    protected $timestamp = false;

    public function tags()
    {
    	$this->belongsToMany(Tag::class,'tag_id');
    }

}
