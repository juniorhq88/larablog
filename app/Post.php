<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Carbon\Carbon;

class Post extends Model
{
    use Sluggable;

    protected $fillable = ['user_id','title','body','published_at'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $dates = ['published_at'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
	{
	    return $this->morphToMany(Tag::class,'taggable');
	}

    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }


    public function getTags()
    {
       
    }

    /**
     * Get the published_at attribute.
     *
     * @param  $date
     * @return string
     */
    public function getPublishedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        return route('post', ['postSlug' => $this->slug]);
    }
}
