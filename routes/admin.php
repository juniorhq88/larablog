<?php

Route::get('/', ['as' => 'dashboard.index', 'uses' => 'DashboardController@index']);
Route::resource('post', 'PostController');
Route::resource('tag', 'TagController');
Route::resource('user', 'UserController');
