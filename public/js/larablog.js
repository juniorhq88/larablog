var larablog = larablog || {};

larablog.pageLoad =
{
	elementLoad:function(){

		if($('.select2').length>0)
	    {
	      jQuery(".select2").select2();
	    }

		$(".select2-multiple").select2();
          jQuery(".js-example-placeholder-single").select2({
              placeholder: "Escriba una Tag",
              allowClear: true
          });

		$("#selectbtn-tag").click(function(){
            $("#selectall-tag > option").prop("selected","selected");
            $("#selectall-tag").trigger("change");
        });
        $("#deselectbtn-tag").click(function(){
            $("#selectall-tag > option").prop("selected","");
            $("#selectall-tag").trigger("change");
        });

	}

}

larablog.event = 
{
    removeItem:function()
    {
        $(document).on("click",'.btn-delete', function (evt) {
            evt.preventDefault();

           var url = $(this).attr('href');

            var data = [];

          	var table = $(this).parents('table');
            var row = $(this).parents('tr');

            data.push({
                name : '_token',
                value : $('meta[name="csrf-token"]').get(0).content
            });
           data.push({
                name : '_method',
                value : 'delete'
            });

            swal({
                  title: "¿Estas seguro de eliminar este elemento?",
                  text: "Esta acción no se podrá deshacer al aceptarlo! No hay Ctrl + Z",
                  type: "error",
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  confirmButtonText: 'Si, de acuerdo!',
                  cancelButtonText: "No, cancelalo!",
              }).then(
              function(isConfirm) {
                  if (isConfirm) {
                  $.ajax({
                      type : 'post',
                      url  : url,
                      cache: false,
                      data: data,
                      headers: { 'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content') },
                      dataType: 'json',
                      success:function(resp)
                      {
                       $(this).parents('table').closest('tr').remove();
                        //table.row(row).remove().draw();
                        swal("", "Elemento eliminado correctamente", "success");
                        window.location.reload();
                      }
                  });
                 }
              });
        });
    },
    insertForm:function(){
    	$(document).on("click",'.btn-submit', function (evt) {
            evt.preventDefault();
            var form = $('#form-new-item');
           var url = form.attr('action');


            $.ajax({
                      type : 'post',
                      url  : url,
                      cache: false,
                      data: form.serialize(),
                      headers: { 'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content') },
                      dataType: 'json',
                      success:function(resp)
                      {


                      	if(resp.status == 'success'){
                      		 swal("", resp.msg, "success");
                      		 $('#form-new-item')[0].reset();
                      		 
                      		 setTimeout(function(){
                      		 	$('.alert').hide();
                      		 	window.location.reload();
                      		 },1500);

                      	}
                      }
                  });

       });
    },
    updateForm:function(){
    	$(document).on("click",'.btn-update', function (evt) {
            evt.preventDefault();
            var form = $('#form-new-item');
           var url = form.attr('action');

            var data = [];

            $.ajax({
                      type : 'PUT',
                      url  : url,
                      cache: false,
                      data: form.serialize(),
                      headers: { 'X-CSRF-TOKEN' : $('meta[name="_token"]').attr('content') },
                      dataType: 'json',
                      success:function(resp)
                      {

                      	if(resp.status == 'success'){
                      		 swal("", resp.msg, "success");
                      		 $('#form-new-item')[0].reset();
                      		 
                      		 setTimeout(function(){
                      		 	$('.alert').hide();
                      		 	window.location.reload();
                      		 },1500);

                      	}
                      }
                  });

       });
    }

  }  


$(function(){
	larablog.pageLoad.elementLoad();
	larablog.event.removeItem();
	larablog.event.insertForm();
	larablog.event.updateForm();
});