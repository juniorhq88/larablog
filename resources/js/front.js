require('./bootstrap');

window.Vue = require('vue');
//import VueRouter from 'vue-router';
import moment from 'moment';
import VueProgressBar from 'vue-progressbar';

const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'left',
  inverse: false
}

Vue.use(VueProgressBar, options);

Vue.component('post-list', require('./components/posts/PostIndex.vue').default);
Vue.component('post-detail', require('./components/posts/PostDetail.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));


axios.defaults.baseURL = document.head.querySelector('meta[name="api-base-url"]').content + '/api';

const app = new Vue({
    el: '#appFront'
});
