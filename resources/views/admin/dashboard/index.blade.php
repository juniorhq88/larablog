@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                <div class="col-sm-12 text-center">
                    <a href="{{ route('admin.user.index') }}" class="btn btn-success pull-right">Listado de Usuarios</a>
                    <a href="{{ route('admin.post.index') }}" class="btn btn-success pull-right">Listado de Posts</a>
                    <a href="{{ route('admin.tag.index') }}" class="btn btn-success pull-right">Listado de Tags</a>
                </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection
