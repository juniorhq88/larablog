@include('errors.validation')
<div class="col-md-8">
  <div class="card card-solid">
    <div class="card-header with-border">
      @if(Request::is('admin/user/*/edit'))
      <h3 class="card-title">Actualizar Usuario</h3>
      @else
      <h3 class="card-title">Crear Usuario</h3>
      @endif
    </div>
    <div class="card-body">
      <div class="form-group">
        <label for="name">Nombre</label>
        {!!Form::text('name',null,['class'=>'form-control', 'required' => 'required','placeholder'=>''])!!}
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        {!!Form::email('email',null,['class'=>'form-control', 'required' => 'required'])!!}
      </div>
      <div class="form-group">
        @if(Request::is('admin/user/*/edit'))
          <label>Contraseña (Opcional)</label>
          {!!Form::password('password',['class'=>'form-control','minlength' => '6'])!!}
        @else
          <label>Contraseña</label>
          {!!Form::password('password',['class'=>'form-control', 'required' => 'required','minlength' => '6'])!!}
        @endif

      </div>
      <div class="form-group">
        @if(Request::is('admin/user/*/edit'))
          <label>Confirmar Contraseña  (Opcional)</label>
          {!!Form::password('password_confirmation',['class'=>'form-control'])!!}
        @else
          <label>Confirmar Contraseña</label>
          {!!Form::password('password_confirmation',['class'=>'form-control', 'required' => 'required'])!!}
        @endif  
      </div>
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="card-body">
    <div class="form-group">
      <img src="{{ asset('img/avatar.png') }}" alt="">
    </div>
  </div>
</div>