@extends('layouts.app')
@section('content')
{!!Form::model($user,['route'=> ['admin.user.update',$user->id],'method'=>'PUT','files' => true, 'data-toggle'=>'validator','role'=>'form','id'=>'form-new-item'])!!}
<section class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><a class="btn btn-default btn-xs" href="{{ route('admin.user.index') }}"><< Ver usuarios</a></h3>
					<div class="card-tools pull-right">
						{!!Form::submit('Actualizar',['class'=>'btn btn-block btn-primary btn-update'])!!}
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-12">
			<div class="row">
				@include('admin.users.forms.index',['user' => $user])
			</div>
		</div>
	</div>
</section>
{!!Form::close()!!}
@endsection