@extends('layouts.app')

@section('title')
Administración de Usuarios
@endsection

@section('content')
<section class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					Listado de Usuarios
					<div class="card-tools pull-right">
					<a class="btn btn-success " href="{{ url('/admin') }}"><< Regresar al Administrador</a>
					<a class="btn btn-success pull-right" href="{{ route('admin.user.create') }}">Nuevo Usuario</a>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-hovered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Email</th>
						<th>Acciones</th>
					</tr>
					
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{ $user->name }}</td>
							</td>
							<td>{{ $user->email }}</td>
							<td>
							        <a href="{{ route('admin.user.edit', $user->id) }}" class="btn btn-info">Editar</a>
							        <a href="{{ route('admin.user.destroy', $user->id) }}" class="btn btn-danger btn-delete">Eliminar</a>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
				</div>
			</div>
		
			
		</div>
	</div>
</section>

@endsection