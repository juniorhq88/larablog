@include('errors.validation')
<div class="col-md-8">
  <div class="card card-solid">
    <div class="card-header with-border">
      @if(Request::is('admin/tag/*/edit'))
      <h3 class="card-title">Actualizar Tag</h3>
      @else
      <h3 class="card-title">Crear Tag</h3>
      @endif
    </div>
    <div class="card-body">
      <div class="form-group">
        <label for="name">Nombre de la Tag</label>
        {!!Form::text('name',null,['class'=>'form-control', 'required' => 'required','placeholder'=>''])!!}
      </div>
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="card-body">
    <div class="form-group">
      </>
    </div>
  </div>
</div>