@extends('layouts.app')

@section('title')
Administración de Tags
@endsection

@section('content')
<section class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					Listado de Tags
					<div class="card-tools pull-right">
					<a class="btn btn-success " href="{{ url('/admin') }}"><< Regresar al Administrador</a>
					<a class="btn btn-success pull-right" href="{{ route('admin.tag.create') }}">Nuevo Tags</a>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-hovered">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Acciones</th>
					</tr>
					
				</thead>
				<tbody>
					@foreach($tags as $tag)
						<tr>
							<td>{{ $tag->name }}</td>
							</td>
							<td>
							        <a href="{{ route('admin.tag.edit', $tag->id) }}" class="btn btn-info">Editar</a>
							         <a href="{{ route('admin.tag.destroy', $tag->id) }}" class="btn btn-danger btn-delete">Eliminar</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
				</div>
			</div>
		
			
		</div>
	</div>
</section>

@endsection