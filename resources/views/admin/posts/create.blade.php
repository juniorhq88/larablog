@extends('layouts.app')
@section('content')
{!!Form::open(['route'=>'admin.post.store', 'method'=>'POST', 'data-toggle'=>'validator','role'=>'form','files' => true,'id'=>'form-new-item'])!!}
<section class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title"><a class="btn btn-default btn-xs" href="{{ route('admin.post.index') }}"><< Ver Posts</a></h3>
					<div class="card-tools pull-right">
						{!!Form::submit('Crear',['class'=>'btn btn-block btn-primary btn-submit'])!!}
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-12">
			<div class="row">
				@include('admin.posts.forms.index')
			</div>
		</div>
	</div>
</section>
{!!Form::close()!!}
@endsection