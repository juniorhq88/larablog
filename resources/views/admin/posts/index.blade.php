@extends('layouts.app')

@section('title')
Administración de Posts
@endsection

@section('content')
<section class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<div class="card">
				<div class="card-header">
					Listado de Posts
					<div class="card-tools pull-right">
					<a class="btn btn-success " href="{{ url('/admin') }}"><< Regresar al Administrador</a>
					<a class="btn btn-success pull-right" href="{{ route('admin.post.create') }}">Nuevo Post</a>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-bordered table-hovered">
				<thead>
					<tr>
						<th>Titulo</th>
						<th>Autor</th>
						<th>Tags</th>
						<th>Acciones</th>
					</tr>
					
				</thead>
				<tbody>
					@foreach($posts as $post)
						<tr>
							<td>{{ $post->title }}</td>
							</td>
							<td>{{ $post->user->name }}</td>

							<td>
								@if(count($post->tags) >=1)	
								@foreach($post->tags as $tag)	
									<span class="btn btn-sm btn-info">{{ $tag->name }}</span>
								@endforeach
								@endif
							</td>
							<td>
							        <a href="{{ route('admin.post.edit', $post->id) }}" class="btn btn-info">Editar</a>
							         <a href="{{ route('admin.post.destroy', $post->id) }}" class="btn btn-danger btn-delete">Eliminar</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
				</div>
			</div>
		
			
		</div>
	</div>
</section>

@endsection