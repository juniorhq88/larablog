@include('errors.validation')
<div class="col-md-8">
  <div class="card card-solid">
    <div class="card-header with-border">
      @if(Request::is('admin/post/*/edit'))
      <h3 class="card-title">Actualizar Post</h3>
      @else
      <h3 class="card-title">Crear Post</h3>
      @endif
    </div>
    <input type="hidden" id="user_id" name="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" id="published_at" name="published_at" value="{{ \Carbon\Carbon::now() }}">
    <div class="card-body">
      <div class="form-group">
        <label for="name">Titulo</label>
        {!!Form::text('title',null,['class'=>'form-control', 'required' => 'required','placeholder'=>''])!!}
      </div>
      <div class="form-group">
        <label for="body">Contenido</label>
        {!!Form::textarea('body',null,['class'=>'form-control', 'required' => 'required'])!!}
      </div>

      
    </div>
  </div>
</div>
<div class="col-md-4">
  <div class="card card-solid">
  <div class="card-body">
    <div class="form-group">
      <label for="tag" class="col-md-12">Seleccione los tags</label>
      {!! Form::select('tag[]', $tags, old('tag'), ['class' => 'form-control col-md-12 select2', 'multiple' => 'multiple', 'id' => 'selectall-tag' ]) !!}

      </div>
  </div>
</div>
</div>