@extends('layouts.front')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">

                <div class="card-body">

                <div class="col-sm-12 text-center">

                    <post-detail></post-detail>

                </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection