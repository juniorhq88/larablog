<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
          DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin',
            'email' => 'admin@larablog.com',
            'password' => Hash::make('password'),
            'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
        ]);
    }
}
